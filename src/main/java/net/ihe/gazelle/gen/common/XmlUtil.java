package net.ihe.gazelle.gen.common;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

public class XmlUtil {

	private static final Charset UTF_8 = Charset.forName("UTF-8");

	public static Document parse(String document) throws Exception {
		ByteArrayInputStream bais = new ByteArrayInputStream(document.getBytes(UTF_8));

		// Rebuild the document
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		builderFactory.setNamespaceAware(true);

		DocumentBuilder builder = builderFactory.newDocumentBuilder();
		Document documentDOM = builder.parse(bais);
		return documentDOM;
	}

}
