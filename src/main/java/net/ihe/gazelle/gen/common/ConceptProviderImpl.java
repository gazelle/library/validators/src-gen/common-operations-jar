package net.ihe.gazelle.gen.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ConceptProviderImpl implements ConceptProvider {
	
	private static final Map<String, List<Concept>> LIST_CONCEPTS = new HashMap<String, List<Concept>>(); 

	@Override
	public List<Concept> getConceptFromValueSet(String valueSetId, String conceptCode, String conceptCodeSystem,
			String lang, String version) {
		if (conceptCode == null || conceptCode.trim().equals("")) {
			return new ArrayList<Concept>();
		}
		if (LIST_CONCEPTS.containsKey(valueSetId) && listConceptContainsCode(LIST_CONCEPTS.get(valueSetId), conceptCode, conceptCodeSystem)) {
			return LIST_CONCEPTS.get(valueSetId);
		}
		String svsRepository = getSVSRepositoryUrl();
		ClientRequest request = configureClientHttp(valueSetId, conceptCode, conceptCodeSystem, lang, version, svsRepository);
		
		try {
			ClientResponse<String> response = request.get(String.class);
			if (response.getStatus() == 200)
			{
				String xmlContent = response.getEntity();
				Document document = XmlUtil.parse(xmlContent);
				NodeList concepts = document.getElementsByTagName("Concept");
				if (concepts.getLength() > 0)
				{
					List<Concept> conceptsList = new ArrayList<Concept>();
					for(int index = 0; index < concepts.getLength(); index ++)
					{
						Node conceptNode = concepts.item(index);
						NamedNodeMap attributes = conceptNode.getAttributes();
						Concept aa = new Concept();
						if (attributes.getNamedItem("code") != null) aa.setCode(attributes.getNamedItem("code").getTextContent());
						if (attributes.getNamedItem("displayName") != null) aa.setDisplayName(attributes.getNamedItem("displayName").getTextContent());
						if (attributes.getNamedItem("codeSystem") != null) aa.setCodeSystem(attributes.getNamedItem("codeSystem").getTextContent());
						if (attributes.getNamedItem("codeSystemName") != null) aa.setCodeSystemName(attributes.getNamedItem("codeSystemName").getTextContent());

						conceptsList.add(aa);
					}
					LIST_CONCEPTS.put(valueSetId + lang, conceptsList);
					return conceptsList;					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<Concept>();
	}

	public ClientRequest configureClientHttp(String valueSetId, String conceptCode, String conceptCodeSystem,
			String lang, String version, String svsRepository) {
		ClientRequest request = new ClientRequest(svsRepository);
		request.followRedirects(true);
		request.queryParameter("id", valueSetId);
		if (lang != null && !lang.isEmpty()) {
			request.queryParameter("lang", lang);
		}
		if (version != null && !version.isEmpty()) {
			request.queryParameter("version", version);
		}
		if (conceptCode != null && !conceptCode.isEmpty()) {
			request.queryParameter("code", conceptCode);
		}
		if (conceptCodeSystem != null && !conceptCodeSystem.isEmpty()) {
			request.queryParameter("codeSystem", conceptCodeSystem);
		}
		return request;
	}

	private boolean listConceptContainsCode(List<Concept> list, String conceptCode, String conceptCodeSystem) {
		if (list != null) {
			for (Concept concept : list) {
				if ( (conceptCode!= null && conceptCode.equals(concept.getCode())) && 
						(conceptCodeSystem == null || conceptCodeSystem.equals(concept.getCodeSystem()))
					) {
					return true;
				}
			}
		}
		return false;
	}
	
	protected String getSVSRepositoryUrl() {
		return "https://gazelle.ihe.net/SVSSimulator/rest/RetrieveValueSetForSimulator";
	}

}
