package net.ihe.gazelle.gen.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SVSConsumer implements ValueSetProvider{

	private static final Map<String, List<Concept>> listConcept = new HashMap<String, List<Concept>>(); 
	
	public static void initializeListConceptCache() {
		listConcept.clear();
	}

	/**
	 * 
	 * @param valueSetId
	 * @return
	 */
	@Override
	public List<Concept> getConceptsListFromValueSet(String valueSetId, String lang) {
		return getConceptsListFromValueSet(valueSetId, null, lang);
	}
	
	@Override
	public List<Concept> getConceptsListFromValueSet(String valueSetId, String version, String lang) {
		if (listConcept.get(valueSetId + version + lang) != null) {
			return listConcept.get(valueSetId + version + lang); 
		}
		String svsRepository = getSVSRepositoryUrl();
		ClientRequest request = new ClientRequest(svsRepository);
		request.followRedirects(true);
		request.queryParameter("id", valueSetId);
		if (lang != null && !lang.isEmpty())
		{
			request.queryParameter("lang", lang);
		}
		if (version != null && !version.isEmpty()) {
			request.queryParameter("version", version);
		}
		try {
			ClientResponse<String> response = request.get(String.class);
			if (response.getStatus() == 200)
			{
				String xmlContent = response.getEntity();
				Document document = XmlUtil.parse(xmlContent);
				NodeList concepts = document.getElementsByTagName("Concept");
				if (concepts.getLength() > 0)
				{
					List<Concept> conceptsList = new ArrayList<Concept>();
					for(int index = 0; index < concepts.getLength(); index ++)
					{
						Node conceptNode = concepts.item(index);
						NamedNodeMap attributes = conceptNode.getAttributes();
						Concept aa = new Concept();
						if (attributes.getNamedItem("code") != null) aa.setCode(attributes.getNamedItem("code").getTextContent());
						if (attributes.getNamedItem("displayName") != null) aa.setDisplayName(attributes.getNamedItem("displayName").getTextContent());
						if (attributes.getNamedItem("codeSystem") != null) aa.setCodeSystem(attributes.getNamedItem("codeSystem").getTextContent());
						if (attributes.getNamedItem("codeSystemName") != null) aa.setCodeSystemName(attributes.getNamedItem("codeSystemName").getTextContent());

						conceptsList.add(aa);
					}
					listConcept.put(valueSetId + lang, conceptsList);
					return conceptsList;					
				}
				else
				{
					listConcept.put(valueSetId + lang, null);
					return null;
				}
			}
			else{
				listConcept.put(valueSetId + lang, null);
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		listConcept.put(valueSetId + lang, null);
		return null;
	}

	
	protected String getSVSRepositoryUrl() {
		return "https://gazelle.ihe.net/SVSSimulator/rest/RetrieveValueSetForSimulator";
	}
}
