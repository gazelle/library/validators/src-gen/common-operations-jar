package net.ihe.gazelle.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.gen.common.Concept;
import net.ihe.gazelle.gen.common.SVSConsumer;
import net.ihe.gazelle.gen.common.ValuesetCheckerImpl;

public class ValuesetCheckerImplTest extends ValuesetCheckerImpl {
	
	@Before
	public void init() {
		setValueSetProvider(new ValueSetProviderClass());
		setConceptProvider(null);
	}

	@Test
	public void testGetValueSetProvider() {
		this.setValueSetProvider(new SVSConsumer());
		assertTrue(this.getValueSetProvider() instanceof SVSConsumer);
	}
	
	@Test
	public void testMatchesValueSetWithDisplayName() throws Exception {
		this.setValueSetProvider(new SVSConsumer());
		assertTrue(this.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus)"));
		assertFalse(this.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus) "));
		assertFalse(this.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "avant repat"));
		assertFalse(this.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", null, "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus) "));
		assertFalse(this.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", null));
		assertFalse(this.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139", 
				null, "avant repat"));
		assertTrue(this.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=20151112", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus)"));
		assertTrue(this.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2015:11:12", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus)"));
		assertTrue(this.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2011", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus)")); // there are no list of codes
		assertFalse(this.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2015:11:12", "OOOOOOO", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus)"));
	}
	
	@Test
	public void testMatchesValueSet() throws Exception {
		this.setValueSetProvider(new SVSConsumer());
		assertTrue(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus)"));
		assertTrue(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus) "));
		assertTrue(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "avant repat"));
		assertFalse(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", null, "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus) "));
		assertTrue(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", null));
		assertTrue(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139", 
				null, "avant repat"));
		assertTrue(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=20151112", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus)"));
		assertTrue(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2015:11:12", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus)"));
		assertTrue(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2011", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus)")); 
		assertFalse(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2015:11:12", "xxxxx", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus)"));
		assertTrue(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2011", "AC", "2.16.840.1.113883.5.139", 
				"Timing Event", "before meal (from lat. ante cibus)")); 
	}
	
	@Test
	public void testTwoConceptAreEquals() throws Exception {
		Concept cons1 = new Concept("aa", "bb", "cc", "dd");
		Concept cons2 = new Concept("aa2", "bb2", "cc2", "dd2");
		assertFalse(cons1.equals(cons2));
		assertFalse(this.twoConceptAreEquals(cons1, cons2));
		cons2.setCode("aa");
		assertFalse(cons1.equals(cons2));
		assertFalse(this.twoConceptAreEquals(cons1, cons2));
		cons2.setCodeSystem("cc");
		assertFalse(cons1.equals(cons2));
		assertTrue(this.twoConceptAreEquals(cons1, cons2));
		assertFalse(this.twoConceptAreEquals(null, cons2));
		assertFalse(this.twoConceptAreEquals(cons1, null));
		assertFalse(this.twoConceptAreEquals(null, null));
	}
	
	@Test
	public void testTwoConceptAreEqualsWithDisplayName() throws Exception {
		Concept cons1 = new Concept("aa", "bb", "cc", "dd");
		Concept cons2 = new Concept("aa2", "bb2", "cc2", "dd2");
		assertFalse(cons1.equals(cons2));
		assertFalse(this.twoConceptAreEqualsWithDisplayName(cons1, cons2));
		cons2.setCode("aa");
		assertFalse(cons1.equals(cons2));
		assertFalse(this.twoConceptAreEqualsWithDisplayName(cons1, cons2));
		cons2.setCodeSystem("cc");
		assertFalse(cons1.equals(cons2));
		assertFalse(this.twoConceptAreEqualsWithDisplayName(cons1, cons2));
		cons2.setCodeSystemName("dd");
		cons2.setDisplayName("bb");
		assertTrue(cons1.equals(cons2));
		assertTrue(this.twoConceptAreEqualsWithDisplayName(cons1, cons2));
		assertFalse(this.twoConceptAreEqualsWithDisplayName(cons1, null));
		assertFalse(this.twoConceptAreEqualsWithDisplayName(null, cons2));
		assertFalse(this.twoConceptAreEqualsWithDisplayName(null, null));
	}

}
