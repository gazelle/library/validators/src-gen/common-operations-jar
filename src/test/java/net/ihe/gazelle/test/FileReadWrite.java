package net.ihe.gazelle.test;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class FileReadWrite {

	public static void printDoc(String doc, String name) throws IOException{
		FileWriter fw = new FileWriter(new File(name));
		fw.append(doc);
		fw.close();
	}

	public static String readDoc(String name) throws IOException{
		BufferedReader scanner = new BufferedReader(new FileReader(name));
		String res = "";
		try {
			String line = scanner.readLine();
			while (line != null){
				res = res + line + "\n";
				line = scanner.readLine();
			}
		}
		finally {
			scanner.close();
		}
		return res;
	}

}
