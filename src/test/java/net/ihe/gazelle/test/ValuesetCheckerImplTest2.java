package net.ihe.gazelle.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.gen.common.CommonOperations;
import net.ihe.gazelle.gen.common.Concept;
import net.ihe.gazelle.gen.common.ConceptProviderImpl;
import net.ihe.gazelle.gen.common.SVSConsumer;
import net.ihe.gazelle.gen.common.ValuesetCheckerImpl;

public class ValuesetCheckerImplTest2 extends ValuesetCheckerImpl {
	
	@Before
	public void init() {
		setValueSetProvider(null);
		setConceptProvider(new ConceptProviderImpl());
	}

	@Test
	public void testMatchesValueSetWithDisplayName() throws Exception {
		setValueSetProvider(null);
		setConceptProvider(new ConceptProviderImpl());
		assertTrue(this.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus)"));
		assertFalse(this.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus) "));
		assertFalse(this.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "avant repat"));
		assertFalse(this.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", null, "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus) "));
		assertFalse(this.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", null));
		assertFalse(this.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139", 
				null, "avant repat"));
		assertTrue(this.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=20151112", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus)"));
		assertTrue(this.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2015:11:12", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus)"));
		assertTrue(this.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2011", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus)")); // there are no list of codes
		assertFalse(this.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.43&version=2015:11:12", "OOOOOOO", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus)"));
	}
	
	@Test
	public void testMatchesCodeToValueSet() throws Exception {
		setValueSetProvider(null);
		setConceptProvider(new ConceptProviderImpl());
		assertTrue(this.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC"));
		assertFalse(this.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "aaaazzzzzzzz"));
		assertTrue(this.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=20151112", "AC"));
		assertTrue(this.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=201511:12", "AC"));
		assertTrue(this.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2011", "AC")); // true because it does not find a codeSystme
		assertFalse(this.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=20151112", "xxxxxxxxxx"));
	}
	
	@Test
	public void testMatchesValueSet() throws Exception {
		setValueSetProvider(null);
		setConceptProvider(new ConceptProviderImpl());
		assertTrue(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus)"));
		assertTrue(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus) "));
		assertTrue(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "avant repat"));
		assertFalse(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", null, "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus) "));
		assertTrue(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", null));
		assertTrue(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139", 
				null, "avant repat"));
		assertTrue(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=20151112", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus)"));
		assertTrue(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2015:11:12", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus)"));
		assertTrue(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2011", "AC", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus)")); 
		assertFalse(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2015:11:12", "xxxxx", "2.16.840.1.113883.5.139", 
				"TimingEvent", "before meal (from lat. ante cibus)"));
		assertTrue(this.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2011", "AC", "2.16.840.1.113883.5.139", 
				"Timing Event", "before meal (from lat. ante cibus)")); 
	}
	
}
