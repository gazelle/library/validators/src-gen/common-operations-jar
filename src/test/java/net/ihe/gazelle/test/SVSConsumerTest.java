package net.ihe.gazelle.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.gen.common.Concept;
import net.ihe.gazelle.gen.common.SVSConsumer;

public class SVSConsumerTest {

	//@Test
	public void test() {
		SVSConsumer consumer = new SVSConsumer() {

			@Override
			protected String getSVSRepositoryUrl() {
				return "https://k-project.ihe-europe.net/SVSSimulator/rest/RetrieveValueSetForSimulator";
			}
		};
		List<Concept> concepts = consumer.getConceptsListFromValueSet("2.16.840.1.113883.3.3731.1.201.8", null);
		Assert.assertNotNull(concepts);
	}

}
