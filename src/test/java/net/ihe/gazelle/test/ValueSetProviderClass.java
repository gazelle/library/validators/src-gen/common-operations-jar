package net.ihe.gazelle.test;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.ihe.gazelle.gen.common.Concept;
import net.ihe.gazelle.gen.common.ValueSetProvider;
import net.ihe.gazelle.gen.common.XmlUtil;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ValueSetProviderClass implements ValueSetProvider{
	
	private static String VALUE_SET_REPOSITORY = "src/test/resources/svs";
	
	public static Map<String, List<Concept>> listConcept = new HashMap<String, List<Concept>>(); 

	@Override
	public List<Concept> getConceptsListFromValueSet(String valueSetId, String lang) {
		return getConceptsListFromValueSet(valueSetId, null, lang);
	}
	
	@Override
	public List<Concept> getConceptsListFromValueSet(String valueSetId, String version, String lang) {
		if (listConcept.get(valueSetId + version + lang) != null) {
			return listConcept.get(valueSetId + version + lang); 
		}
		try {
			String filePath =  VALUE_SET_REPOSITORY + File.separator + valueSetId;
			if (version != null) {
				filePath += "-" + version.replace(":", "");
			}
			filePath += ".xml";
			String content = FileReadWrite.readDoc(filePath);
			Document document = XmlUtil.parse(content);
			NodeList concepts = document.getElementsByTagName("Concept");
			if (concepts.getLength() > 0)
			{
				List<Concept> conceptsList = new ArrayList<Concept>();
				for(int index = 0; index < concepts.getLength(); index ++)
				{
					Node conceptNode = concepts.item(index);
					NamedNodeMap attributes = conceptNode.getAttributes();
					Concept aa = new Concept();
					if (attributes.getNamedItem("code") != null) aa.setCode(attributes.getNamedItem("code").getTextContent());
					if (attributes.getNamedItem("displayName") != null) aa.setDisplayName(attributes.getNamedItem("displayName").getTextContent());
					if (attributes.getNamedItem("codeSystem") != null) aa.setCodeSystem(attributes.getNamedItem("codeSystem").getTextContent());
					if (attributes.getNamedItem("codeSystemName") != null) aa.setCodeSystemName(attributes.getNamedItem("codeSystemName").getTextContent());

					conceptsList.add(aa);
				}
				listConcept.put(valueSetId + version + lang, conceptsList);
				return conceptsList;					
			}
			else
			{
				listConcept.put(valueSetId + version + lang, null);
				return null;
			}
			
		} catch (Exception e) {
			listConcept.put(valueSetId + version + lang, null);
			return null;
		}
	}

}
