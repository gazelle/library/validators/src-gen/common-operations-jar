package net.ihe.gazelle.test;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration NullFlavor.
 *
 */

@XmlType(name = "NullFlavor")
@XmlEnum
@XmlRootElement(name = "NullFlavor")
public enum NullFlavor {
	@XmlEnumValue("A.SKU")
	ASKU("A.SKU"),
	@XmlEnumValue("MSK")
	MSK("MSK"),
	@XmlEnumValue("NA")
	NA("NA"),
	@XmlEnumValue("NASK")
	NASK("NASK"),
	@XmlEnumValue("NAV")
	NAV("NAV"),
	@XmlEnumValue("NI")
	NI("NI"),
	@XmlEnumValue("NINF")
	NINF("NINF"),
	@XmlEnumValue("OTH")
	OTH("OTH"),
	@XmlEnumValue("PINF")
	PINF("PINF"),
	@XmlEnumValue("TRC")
	TRC("TRC"),
	@XmlEnumValue("UNK")
	UNK("UNK"),
	@XmlEnumValue("NP")
	NP("NP"),
	@XmlEnumValue("OP")
	OP("OP");
	
	private final String value;

    NullFlavor(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }

    public static NullFlavor fromValue(String v) {
        for (NullFlavor c: NullFlavor.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}