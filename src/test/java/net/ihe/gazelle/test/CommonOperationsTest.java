package net.ihe.gazelle.test;

import net.ihe.gazelle.gen.common.CommonOperations;
import net.ihe.gazelle.gen.common.CommonOperationsStatic;
import net.ihe.gazelle.gen.common.SVSConsumer;
import net.ihe.gazelle.gen.common.ValuesetCheckerImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CommonOperationsTest {

    private CommonOperations co = new CommonOperations();

    @Before
    public void init() {
        CommonOperations.setValueSetProvider(new SVSConsumer());
    }

    @Test
    public void testIsDTM() {
        String ss = "";
        assertFalse(co.isDTM(ss));
        ss = "2005";
        assertTrue(co.isDTM(ss));
        ss = "200501";
        assertTrue(co.isDTM(ss));
        ss = "20050102";
        assertTrue(co.isDTM(ss));
        ss = "2005010203";
        assertTrue(co.isDTM(ss));
        ss = "200501020304";
        assertTrue(co.isDTM(ss));
        ss = "20050102030405";
        assertTrue(co.isDTM(ss));
        ss = "2105";
        assertFalse(co.isDTM(ss));
        ss = "21051";
        assertFalse(co.isDTM(ss));
        ss = "21022";
        assertFalse(co.isDTM(ss));
        ss = "210513";
        assertFalse(co.isDTM(ss));
        ss = "210500";
        assertFalse(co.isDTM(ss));
        ss = "21050100";
        assertFalse(co.isDTM(ss));
        ss = "21050132";
        assertFalse(co.isDTM(ss));
        ss = "2105012225";
        assertFalse(co.isDTM(ss));
        ss = "210501223";
        assertFalse(co.isDTM(ss));
        ss = "2005011200";
        assertTrue(co.isDTM(ss));
        ss = "200501120160";
        assertFalse(co.isDTM(ss));
        ss = "200501120100";
        assertTrue(co.isDTM(ss));
        ss = "20050112017";
        assertFalse(co.isDTM(ss));
        ss = "20050112010000";
        assertTrue(co.isDTM(ss));
        ss = "20050112010060";
        assertFalse(co.isDTM(ss));
        ss = "2005011201004060";
        assertFalse(co.isDTM(ss));
        ss = "200501120100406";
        assertFalse(co.isDTM(ss));
        ss = "20050112000000";
        assertTrue(co.isDTM(ss));
        ss = "20050112010040400";
        assertFalse(co.isDTM(ss));
        ss = "20141209202727";
        assertTrue(co.isDTM(ss));
        ss = "20141209235959";
        assertTrue(co.isDTM(ss));
        ss = "20141209005959";
        assertTrue(co.isDTM(ss));
        ss = "20991209005959";
        assertTrue(co.isDTM(ss));
        ss = "19001209005959";
        assertTrue(co.isDTM(ss));
    }

    @Test
    public void testIsOID() {
        String ss = "";
        assertFalse(co.isOID(ss));
        ss = "1.3.6.1.4.1.21367.2005.3.7";
        assertTrue(co.isOID(ss));
        ss = "0.3.6.0.4.1.21367.2005.3.7";
        assertTrue(co.isOID(ss));
        ss = "0.3.6.0.04.1.21367.2005.3.7";
        assertFalse(co.isOID(ss));
        ss = "0.3.6.0.04.1.21367.2005.3.7ISO";
        assertFalse(co.isOID(ss));
        ss = "01111";
        assertFalse(co.isOID(ss));
        ss = "1.";
        assertFalse(co.isOID(ss));
        ss = "1.3.182.3.1.1.1231231";
        assertTrue(co.isOID(ss));
        ss = "1.234.2.3.4.5";
        assertTrue(co.isOID(ss));
        ss = "0.4.0.127.0.16.1.1.2.1";
        assertTrue(co.isOID(ss));
        ss = "3.4.5";
        assertTrue(co.isOID(ss));
        assertFalse(co.isOID(null));
    }

    @Test
    public void testIsUUID() {
        String ss = "";
        assertFalse(co.isUUID(ss));
        ss = "urn:uuid:9e0110f8-4748-4f1e-b0a8-cecae32209c7";
        assertTrue(co.isUUID(ss));
        ss = "zEEDAAff3143535461234";
        assertFalse(co.isUUID(ss));
        ss = "urn:uuid:";
        assertFalse(co.isUUID(ss));
        ss = "urn:uuid:--gzeifugzefyv";
        assertFalse(co.isUUID(ss));
        ss = "9e0110f8-4748-4f1e-b0a8-cecae32209c7";
        assertTrue(co.isUUID(ss));
        assertFalse(co.isUUID(null));
    }

    @Test
    public void testIsUID() {
        String ss = "urn:uuid:9e0110f8-4748-4f1e-b0a8-cecae32209c7";
        assertTrue(co.isUUID(ss));
        assertTrue(co.isUID("sssssss"));
        ss = "1.234.2.3.4.5";
        assertTrue(co.isUID(ss));
    }

    @Test
    public void testIsTSType() throws Exception {
        assertFalse(co.isTSType(null));
        assertFalse(co.isTSType(""));
        assertTrue(co.isTSType("1986"));
        assertTrue(co.isTSType("198601"));
        assertTrue(co.isTSType("19860125"));
        assertTrue(co.isTSType("1986012501"));
        assertTrue(co.isTSType("198601250101"));
        assertTrue(co.isTSType("1986010100"));
        assertTrue(co.isTSType("1986010159"));
    }

    @Test
    public void testIsCSType() throws Exception {
        assertFalse(co.isCSType(null));
        String ss = "aaaaa";
        assertTrue(co.isCSType(ss));
        ss = "aaaaa ddd";
        assertFalse(co.isCSType(ss));
    }

    @Test
    public void testIsSTType() throws Exception {
        assertFalse(co.isSTType(null));
        assertFalse(co.isSTType(""));
        assertTrue(co.isSTType("eee"));
    }

    @Test
    public void testIsRUID() throws Exception {
        assertFalse(co.isRUID(null));
        assertFalse(co.isRUID("ff ee"));
        assertFalse(co.isRUID(""));
        assertTrue(co.isRUID("sssssss"));
    }

    @Test
    public void testIsXCN() {
        String ss = "";
        assertFalse(co.isXCN(ss));
        ss = "11375^Welby^Marcus^J^Jr. MD^Dr^^^&1.2.840.113619.6.197&ISO";
        assertTrue(co.isXCN(ss));
        ss = "11375^^^J^Jr. MD^Dr^^^&1.2.840.113619.6.197&ISO";
        assertTrue(co.isXCN(ss));
        ss = "^Welby^Marcus^J^Jr. MD^Dr^^^&1.2.840.113619.6.197&ISO";
        assertTrue(co.isXCN(ss));
        // on component 9, the first subcomponent shall be empty
        ss = "^Welby^Marcus^J^Jr. MD^Dr^^^ee&1.2.840.113619.6.197&ISO";
        assertFalse(co.isXCN(ss));
        ss = "11375^^^J^Jr. MD^Dr^^^";
        assertTrue(co.isXCN(ss));
        ss = "11375^^^J^Jr. MD^Dr^";
        assertTrue(co.isXCN(ss));
        ss = "11375^^^J^Jr. MD^Dr";
        assertTrue(co.isXCN(ss));
        ss = "11375^^^J^Jr. MD^";
        assertTrue(co.isXCN(ss));
        ss = "11375^^^J^Jr. MD";
        assertTrue(co.isXCN(ss));
        ss = "11375^^^J^";
        assertTrue(co.isXCN(ss));
        ss = "11375^^^";
        assertTrue(co.isXCN(ss));
        ss = "11375^^";
        assertTrue(co.isXCN(ss));
        ss = "Dr epSOS";
        assertTrue(co.isXCN(ss));
        ss = "^^^J^Jr. MD^Dr^^^&1.2.840.113619.6.197&ISO";
        assertFalse(co.isXCN(ss));
        // on component 9, the second subcomponent must be an ISO OID
        ss = "^Welby^Marcus^J^Jr. MD^Dr^^^&1.2.840.11qqaa3619.6.197&ISO";
        assertFalse(co.isXCN(ss));
        // on component 9, the second subcomponent must be an ISO OID
        ss = "^Welby^Marcus^J^Jr. MD^Dr^^^&1&ISO";
        assertFalse(co.isXCN(ss));
        // on component 9, the second subcomponent must be an ISO OID
        ss = "^Welby^Marcus^J^Jr. MD^Dr^^^&1.2.8&ISS";
        assertFalse(co.isXCN(ss));
        ss = "^Welby^Marcus^J^Jr. MD^Dr^^^&1.2.8&ISO^zszs^^zszzs^^";
        assertTrue(co.isXCN(ss));
        ss = "00B1049465^DOC4946^KIT^^^^^^&1.2.250.1.71.4.2.1&ISO^D^^^IDNPS";
        assertTrue(co.isXCN(ss));
    }

    @Test
    public void testIsXON() {
        String ss = "";
        assertFalse(co.isXON(ss));
        ss = "Some Hospital";
        // XON.6.2 – Assigning Authority Universal Id – this field is required if XON.10 is valued and not an OID
        assertTrue(co.isXON(ss));
        ss = "Some Hospital^^^^^^^^^1.2.3.4.5.6.7.8.9.1789.45";
        assertTrue(co.isXON(ss));
        // XON.6.2 – Assigning Authority Universal Id – this field is required if XON.10 is valued and not an OID
        ss = "Some Hospital^^^^^&1.2.3.4.5.6.7.8.9.1789&ISO^^^^45";
        assertTrue(co.isXON(ss));
        // XON.6.3 – Assigning Authority Universal Id – this field is required if XON.10 is valued and not an OID
        ss = "Some Hospital^^^^^&1.2.3.4.5.6.7.8.9.1789^^^^45";
        assertFalse(co.isXON(ss));
        // XON.6.3 – Assigning Authority Universal Id – this field is required if XON.10 is valued and not an OID
        ss = "Some Hospital^^^^^&1.2.3.4.5.6.7.8.9.1789&DDD^^^^45";
        assertFalse(co.isXON(ss));
        // XON.10 – Organization Identifier – this field is optional
        ss = "Some Hospital^^^^^^^^^";
        assertTrue(co.isXON(ss));
        // No other fields shall be specified
        ss = "Some Hospital^ss^ss^ss^^^^^^";
        assertFalse(co.isXON(ss));
        ss = "Some Hospital^^^^^^";
        assertTrue(co.isXON(ss));
        ss = "Some Hospital^^";
        assertTrue(co.isXON(ss));
        ss = "Some Hospital^";
        assertTrue(co.isXON(ss));
        // XON.1 – Organization Name – this field is required
        ss = "^";
        assertFalse(co.isXON(ss));
        // XON.6.2 – Assigning Authority Universal Id – this field is required if XON.10 is valued and not an OID
        ss = "Some Hospital^^^^^zefzefezfzef^^^^45";
        assertFalse(co.isXON(ss));
        ss = "Some Hospital^^^^^^^^^^^^^^^^^^";
        assertFalse(co.isXON(ss));
        ss = "Some Hospital^^^^^&1.2.3.4.5.6.7.8.9.1789^^^^45";
        assertFalse(co.isXON(ss));
    }

    @Test
    public void testStringMatches() {
        String ss = "123rrrazAA";
        assertTrue("Should be able to test a String against a reg-exp", co.matches(ss, "\\d{3}[a-zA-Z]+"));
    }

    @Test
    public void testStringDontMatch() {
        String ss = "rrrazAA123";
        assertFalse("Should be able to test a String against a reg-exp", co.matches(ss, "\\d{3}[a-zA-Z]+"));
    }

    @Test
    public void testDoubleMatches() {
        double mydouble = -12.32;
        assertTrue("Should be able to test a double against a reg-exp", co.matches(mydouble, "^-?\\d+\\.\\d{2}$"));
    }

    @Test
    public void testDoubleMatches2() {
        double mydouble = -12;
        assertFalse("Unfortunnately, we cannot test integer value stored in double against a reg-exp, as .0 is added at the string convertion",
                co.matches(mydouble, "^-?\\d+$"));
    }

    @Test
    public void testDoubleDontMatch() {
        double mydouble = 3.1457;
        assertFalse("Should be able to test a double against a reg-exp", co.matches(mydouble, "^-?\\d+\\.\\d{2}$"));
    }

    @Test
    public void testIntegerMatches() {
        int myint = -135;
        assertTrue("Should be able to test an integer against a reg-exp", co.matches(myint, "-?\\d+"));
    }

    @Test
    public void testIntegerDontMatch() {
        int myint = 1350;
        assertFalse("Should be able to test an integer against a reg-exp", co.matches(myint, "-?\\d{3}"));
    }

    @Test
    public void testExtractListString() throws Exception {
        List<String> res = CommonOperationsStatic.extractListLitteral(NullFlavor.class);
        assertTrue(res.contains("MSK"));
    }

    @Test
    public void testExtractLitteralValue() throws Exception {
        String res = CommonOperationsStatic.extractLitteralValue(NullFlavor.ASKU);
        assertTrue(res.equals("A.SKU"));
    }

    @Test
    public void testMatchesValueSetWithDisplayName() throws Exception {
        CommonOperations.setValueSetProvider(new SVSConsumer());
        CommonOperations.getValuesetChecker().setConceptProvider(null);
        assertTrue(co.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139",
                "TimingEvent", "before meal (from lat. ante cibus)"));
        assertFalse(co.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139",
                "TimingEvent", "before meal (from lat. ante cibus) "));
        assertFalse(co.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139",
                "TimingEvent", "avant repat"));
        assertFalse(co.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", null, "2.16.840.1.113883.5.139",
                "TimingEvent", "before meal (from lat. ante cibus) "));
        assertFalse(co.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139",
                "TimingEvent", null));
        assertFalse(co.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139",
                null, "avant repat"));
        System.out.println("co==========" + CommonOperations.getValuesetChecker());
        System.out.println("co==========" + ((ValuesetCheckerImpl) CommonOperations.getValuesetChecker()).getValueSetProvider());
        System.out.println("co==========" + ((ValuesetCheckerImpl) CommonOperations.getValuesetChecker()).getConceptProvider());
        assertTrue(co.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=20151112", "AC", "2.16.840.1.113883.5.139",
                "TimingEvent", "before meal (from lat. ante cibus)"));
        assertTrue(co.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2015:11:12", "AC", "2.16.840.1.113883.5.139",
                "TimingEvent", "before meal (from lat. ante cibus)"));
        assertTrue(co.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2011", "AC", "2.16.840.1.113883.5.139",
                "TimingEvent", "before meal (from lat. ante cibus)")); // there are no list of codes
        assertFalse(co.matchesValueSetWithDisplayName("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2015:11:12", "OOOOOOO", "2.16.840.1.113883.5.139",
                "TimingEvent", "before meal (from lat. ante cibus)"));
    }

    @Test
    public void testMatchesCodeToValueSet() throws Exception {
        CommonOperations.setValueSetProvider(new SVSConsumer());
        CommonOperations.getValuesetChecker().setConceptProvider(null);
        assertTrue(co.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC"));
        assertFalse(co.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "aaaazzzzzzzz"));
        assertTrue(co.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=20151112", "AC"));
        assertTrue(co.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=201511:12", "AC"));
        assertTrue(co.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2011", "AC")); // true because it does not find a codeSystme
        assertFalse(co.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=20151112", "xxxxxxxxxx"));
    }

    @Test
    public void testMatchesValueSet() throws Exception {
        CommonOperations.setValueSetProvider(new SVSConsumer());
        CommonOperations.getValuesetChecker().setConceptProvider(null);
        assertTrue(co.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139",
                "TimingEvent", "before meal (from lat. ante cibus)"));
        assertTrue(co.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139",
                "TimingEvent", "before meal (from lat. ante cibus) "));
        assertTrue(co.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139",
                "TimingEvent", "avant repat"));
        assertFalse(co.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", null, "2.16.840.1.113883.5.139",
                "TimingEvent", "before meal (from lat. ante cibus) "));
        assertTrue(co.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139",
                "TimingEvent", null));
        assertTrue(co.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41", "AC", "2.16.840.1.113883.5.139",
                null, "avant repat"));
        assertTrue(co.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=20151112", "AC", "2.16.840.1.113883.5.139",
                "TimingEvent", "before meal (from lat. ante cibus)"));
        assertTrue(co.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2015:11:12", "AC", "2.16.840.1.113883.5.139",
                "TimingEvent", "before meal (from lat. ante cibus)"));
        assertTrue(co.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2011", "AC", "2.16.840.1.113883.5.139",
                "TimingEvent", "before meal (from lat. ante cibus)"));
        assertFalse(co.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2015:11:12", "xxxxx", "2.16.840.1.113883.5.139",
                "TimingEvent", "before meal (from lat. ante cibus)"));
        assertTrue(co.matchesValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.41&version=2011", "AC", "2.16.840.1.113883.5.139",
                "Timing Event", "before meal (from lat. ante cibus)"));
    }

}
