package net.ihe.gazelle.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.ihe.gazelle.gen.common.Concept;
import net.ihe.gazelle.gen.common.SVSConsumer;
import net.ihe.gazelle.gen.common.XmlUtil;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TestSVS {
	
	static final String VALUE_SET_REPOSITORY = "/home/aboufahj/SVSRepository/";
	
	public static void main(String[] args) {
		File fil = new File(VALUE_SET_REPOSITORY);
		Map<String, List<Concept>> listNotWellConcept = new HashMap<String, List<Concept>>();
		for (File fis : fil.listFiles()) {
			//if (fis.getName().contains("1.3.6.1.4.1.21367.101.104")){
				verifyThatTheFilIsIntegratedOnNewSVS(fis, listNotWellConcept);
			//}
		}
		System.out.println(fil.listFiles().length + " --- " + listNotWellConcept.size());
		printlistNotWellConcept(listNotWellConcept);
	}

	private static void printlistNotWellConcept(Map<String, List<Concept>> listNotWellConcept) {
		for (String str : listNotWellConcept.keySet()) {
			System.out.print(str + ",");
			for (Concept conc : listNotWellConcept.get(str)) {
				System.out.print(conc.getCode() + ",");
			}
			System.out.println();
		}
		
	}

	private static void verifyThatTheFilIsIntegratedOnNewSVS(File fil, Map<String, List<Concept>> listNotWellConcept) {
		 List<Concept> llocal = getConceptsListFromValueSet(getValueSet(fil), getLang(fil));
		 if (llocal == null){
			 llocal = new ArrayList<Concept>();
		 }
		 SVSConsumer consumer = new SVSConsumer(){
			 @Override
			protected String getSVSRepositoryUrl() {
				return "http://gazelle.ihe.net/SVSSimulator/rest/RetrieveValueSetForSimulator";
			}
		 };
		 List<Concept> lextern = consumer.getConceptsListFromValueSet(getValueSet(fil), getLangFrModified(fil));
		 boolean containsAll = lextern!= null && lextern.containsAll(llocal);
		 if (!containsAll){
			 if (lextern != null){
				 llocal.removeAll(lextern);
			 }
			 listNotWellConcept.put(getValueSet(fil) + "_" + getLang(fil), llocal);
		 }
	}
	
	private static String getLangFrModified(File fil) {
		String lg = getLang(fil);
		if (lg != null && lg.equals("fr")) lg = "fr-FR";
		return lg;
	}

	private static String getValueSet(File fil) {
		if (fil.getName() != null){
			return fil.getName().replaceAll("_.*", "").replace(".xml", "");
		}
		return null;
	}

	private static String getLang(File fil) {
		Pattern pat = Pattern.compile(".*?_(.*?).xml");
		Matcher mat = pat.matcher(fil.getName());
		if (mat.find()){
			return mat.group(1);
		}
		return null;
	}

	public static String readDoc(String name) throws IOException {
		BufferedReader scanner = new BufferedReader(new InputStreamReader(new FileInputStream(name), "UTF-8"));
		StringBuilder res = new StringBuilder();
		try {
			String line = scanner.readLine();
			while (line != null) {
				res.append(line + "\n");
				line = scanner.readLine();
			}
		} finally {
			scanner.close();
		}
		return res.toString();
	}
	
	private static List<Concept> getConceptsListFromValueSet(String valueSetId, String lang) {
		try {
			String langs = lang==null?"":"_"+lang;
			String content = readDoc(VALUE_SET_REPOSITORY + File.separator + valueSetId + langs + ".xml");
			Document document = XmlUtil.parse(content);
			NodeList concepts = document.getElementsByTagName("Concept");
			if (concepts.getLength() > 0)
			{
				List<Concept> conceptsList = new ArrayList<Concept>();
				for(int index = 0; index < concepts.getLength(); index ++)
				{
					Node conceptNode = concepts.item(index);
					NamedNodeMap attributes = conceptNode.getAttributes();
					Concept aa = new Concept();
					if (attributes.getNamedItem("code") != null) aa.setCode(attributes.getNamedItem("code").getTextContent());
					if (attributes.getNamedItem("displayName") != null) aa.setDisplayName(attributes.getNamedItem("displayName").getTextContent());
					if (attributes.getNamedItem("codeSystem") != null) aa.setCodeSystem(attributes.getNamedItem("codeSystem").getTextContent());
					if (attributes.getNamedItem("codeSystemName") != null) aa.setCodeSystemName(attributes.getNamedItem("codeSystemName").getTextContent());

					conceptsList.add(aa);
				}
				return conceptsList;					
			}
			else
			{
				return null;
			}
			
		} catch (Exception e) {
			return null;
		}
	}


}
