package net.ihe.gazelle.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import net.ihe.gazelle.gen.common.CommonOperationsStatic;

import org.junit.Test;

public class COSTest {

	@Test
	public void testExtractNumberDigits() {
		int nd = CommonOperationsStatic.extractNumberDigits(12.22);
		assertTrue(nd == 2);
		nd = CommonOperationsStatic.extractNumberDigits(12d);
		assertTrue(nd == 1); // 12d = 12.0
		nd = CommonOperationsStatic.extractNumberDigits(12.0);
		assertTrue(nd == 1);
	}

	@Test
	public void testIsLowerOrEqual() {
		assertTrue(CommonOperationsStatic.isLowerOrEqual(12.0, "12"));
		assertTrue(CommonOperationsStatic.isLowerOrEqual(12.0, "13"));
		assertTrue(CommonOperationsStatic.isLowerOrEqual(12.1, "12.15"));
		assertFalse(CommonOperationsStatic.isLowerOrEqual(12.1, "12.05"));
	}

	@Test
	public void testIsGreaterOrEqual() {
		assertTrue(CommonOperationsStatic.isGreaterOrEqual(12.0, "12"));
		assertFalse(CommonOperationsStatic.isGreaterOrEqual(12.0, "13"));
		assertFalse(CommonOperationsStatic.isGreaterOrEqual(12.1, "12.15"));
		assertTrue(CommonOperationsStatic.isGreaterOrEqual(12.1, "12.05"));
	}
	
	@Test
	public void testExtractFractionDigits() throws Exception {
		String dig = CommonOperationsStatic.extractFractionDigits(12d);
		assertTrue(dig.equals("0"));
		dig = CommonOperationsStatic.extractFractionDigits(12.0);
		assertTrue(dig.equals("0"));
		dig = CommonOperationsStatic.extractFractionDigits(12.123);
		assertTrue(dig.equals("123"));
		dig = CommonOperationsStatic.extractFractionDigits(0.23d);
		assertTrue(dig.equals("23"));
		dig = CommonOperationsStatic.extractFractionDigits(1.000000);
		assertTrue(dig.equals("0"));
		Double db = new Double("1.0000");
		dig = CommonOperationsStatic.extractFractionDigits(db);
		assertTrue(dig.equals("0"));
		dig = CommonOperationsStatic.extractFractionDigits(1.001);
		assertTrue(dig.equals("001"));
	}

}
